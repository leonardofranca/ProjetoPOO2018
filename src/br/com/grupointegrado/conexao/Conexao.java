/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Leonardo
 */
public class Conexao {

    private static Conexao instancy;
    private Connection conexao = null;
    private String url;
    private String driver;
    private String usuario;
    private String senha;
    public ResultSet resultSet;
    public Statement statement;

    public Conexao() throws ClassNotFoundException, SQLException {

        this.url = "jdbc:oracle:thin:@localhost:1521:XE";
        this.driver = "oracle.jdbc.driver.OracleDriver";
        this.usuario = "trabpoo";
        this.senha = "trabpoo";

        try {
            Class.forName(this.driver);
            System.out.println("Driver carregado com Sucesso");
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException("Driver do banco de dados não localizado");
        }
        try {
            this.conexao = DriverManager.getConnection(
                    this.url,
                    this.usuario,
                    this.senha
            );
            System.out.println("Conectado com Sucesso");
        } catch (SQLException e) {
            throw new SQLException("Erro ao conectar ao banco");
        }
    }

    public static Conexao getInstancy() {
        try {
            instancy = new Conexao();
            return instancy;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void desconecta() {
        try {
            if (conexao != null) {

                conexao.close();
                System.out.println("Conexao fechada com sucesso!!!!");
            } else {
                System.out.println("Não existe conexão");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public Connection getConexao() {
        return conexao;
    }

    public static void main(String[] args) {

        try {
            Conexao c = new Conexao();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
     public void executeSQL(String sql) {

        try {

            statement = conexao.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

            resultSet = statement.executeQuery(sql);

        } catch (SQLException ex) {

            ex.printStackTrace();
            resultSet = null;
        }

    }
}
