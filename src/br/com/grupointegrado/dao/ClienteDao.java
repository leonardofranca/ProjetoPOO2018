/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.dao;

import br.com.grupointegrado.model.Cliente;
import br.com.grupointegrado.util.UltimoCodigo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public class ClienteDao extends Dao {

    public ClienteDao() {
    }
    public void inserir(Cliente cliente) throws SQLException {

        
        try {
            String SQL = "INSERT INTO CAD_CLIENTE "
                    + "(CD_CLIENTE, NOME, CPF, RG, SEXO, IN_SITUACAO, ENDERECO, BAIRRO, CEP, CIDADE, UF, TELEFONE, CELULAR)"
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            cliente.setCdCliente(new UltimoCodigo().getUltimo("CAD_CLIENTE", "CD_CLIENTE"));
            super.ps = super.conexao.prepareStatement(SQL);
            

            cliente.setCpf(cliente.getCpf().replace(".", ""));
            cliente.setRg(cliente.getRg().replace("-", ""));
            super.ps.setInt(1, cliente.getCdCliente());
            super.ps.setString(2, cliente.getNome());
            super.ps.setString(3, cliente.getCpf());
            super.ps.setString(4, cliente.getRg());
            super.ps.setInt(5, cliente.getSituacao());
            super.ps.setString(6, cliente.getEndereco());
            super.ps.setString(7, cliente.getBairro());
            super.ps.setString(8, cliente.getBairro());
            super.ps.execute();

        } catch (SQLException ex) {

            ex.printStackTrace();

        } finally {

            super.ps.close();
        }

    }

    public Cliente findOne(Cliente cliente) throws Exception{
        
        String sql = "select * from CAD_CLIENTE WHERE CD_CLIENTE = ?";
        super.ps = super.conexao.prepareStatement(sql);
        
        super.ps.setInt(1, cliente.getCdCliente());
        super.executeSQL();
        
        return getObjeto(super.resultSet);
    }

    public Cliente getObjeto(ResultSet resultSet) throws SQLException {

        Cliente cli = new Cliente();
        if (resultSet.next()) {
            cli.setCdCliente(resultSet.getInt("CD_CLIENTE"));
            cli.setNome(resultSet.getString("NOME"));
            cli.setCidade(resultSet.getString("CIDADE"));
            cli.setBairro(resultSet.getString("BAIRRO"));
            cli.setCelular(resultSet.getString("CELULAR"));
            cli.setCep(resultSet.getString("CEP"));
            cli.setUf(resultSet.getString("UF"));
            cli.setCpf(resultSet.getString("CPF"));
            cli.setEndereco(resultSet.getString("ENDERECO"));
            cli.setRg(resultSet.getString("RG"));
            cli.setSexo(resultSet.getString("SEXO"));
            cli.setSituacao(resultSet.getInt("IN_SITUACAO"));
            cli.setTelefone(resultSet.getString("TELEFONE"));
            return cli;
        }
        return null;

    }

    public static void main(String[] args) throws SQLException, Exception {
        Cliente cli = new Cliente();
        ClienteDao clidao = new ClienteDao();
        cli.setCdCliente(1);

        System.out.println(clidao.findOne(cli).toString());

    }

}
