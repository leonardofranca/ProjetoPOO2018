/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.dao;

import br.com.grupointegrado.conexao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 *
 * @author Leonardo
 */
public class Dao {
    protected Connection conexao;
    final Conexao c;
    protected PreparedStatement ps;
    protected ResultSet resultSet;
    private ResultSetMetaData metaData;

    public Dao() {
        c  = Conexao.getInstancy();
        this.conexao = c.getConexao();
    }
   
    protected void insert() throws SQLException{
        this.ps.executeUpdate();
        
    }
    protected void update()throws SQLException{
        this.ps.executeUpdate();
    }
    protected void delete()throws SQLException{
        this.ps.executeUpdate();
    }
    protected void executeSQL(){
        try {
            resultSet = this.ps.executeQuery();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            metaData = this.ps.getMetaData();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
