/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.dao;

import br.com.grupointegrado.model.Produto;
import br.com.grupointegrado.util.UltimoCodigo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 *
 * @author Leonardo
 */
public class ProdutoDao extends Dao {

    public ProdutoDao() {
    }

    public void insert(Produto prod) throws SQLException {

        prod.setIdProduto(new UltimoCodigo().getUltimo("CAD_PRODUTO", "ID_PRODUTO"));

        System.out.println("CODIGO UNI " + prod.getIdProduto());
        System.out.println("CODIGO UNI " + prod.getIdUniMedida().getIdUniMedida());

        String SQL = "insert into CAD_Produto("
                + "id_PRODUTO,ID_unimedida,DEScricao,Vl_unitario,QT_estoque) "
                + "values(?,?,?,?,?)";

        super.ps = super.conexao.prepareStatement(SQL);

        super.ps.setInt(1, prod.getIdProduto());
        super.ps.setInt(2, prod.getIdUniMedida().getIdUniMedida());
        try {
            super.ps.setString(3, prod.getDescProduto());
        } catch (NullPointerException e) {
            super.ps.setNull(3, Types.VARCHAR);
        }
        super.ps.setDouble(4, prod.getVlUnitario());
        super.ps.setDouble(5, prod.getQtEstoque());

        super.insert();
        super.ps.close();
    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }

    public void findOne() {

    }

    public void findAll() {

    }

    public void findDescricao() {

    }

    public Produto findById(Integer cod) throws SQLException {

        String SQL = "SELECT * FROM CAD_PRODUTO WHERE ID_PRODUTO = " + cod;
        super.ps = super.conexao.prepareStatement(SQL);
        
        super.executeSQL();

       
        return getObjeto(super.resultSet);

    }

    public Produto getObjeto(ResultSet resultSet) throws SQLException {

        Produto pro = new Produto();
        UnidadeMedidaDao uniDao = new UnidadeMedidaDao();
        if (resultSet.next()) {
            pro.setIdProduto(resultSet.getInt("ID_PRODUTO"));
            pro.setDescProduto(resultSet.getString("DESCRICAO"));
            pro.setIdUniMedida(uniDao.buscarPorCodigo(resultSet.getInt("ID_UNIMEDIDA")));
            pro.setQtEstoque(resultSet.getDouble("QT_ESTOQUE"));
            pro.setVlUnitario(resultSet.getDouble("VL_UNITARIO"));
            return pro;
        }
        return null;

    }
}
