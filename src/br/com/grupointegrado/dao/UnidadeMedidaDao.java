/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.dao;

import br.com.grupointegrado.model.UnidadeMedida;
import br.com.grupointegrado.util.UltimoCodigo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public class UnidadeMedidaDao extends Dao {

    public UnidadeMedidaDao() {
    }

    public void insert(UnidadeMedida uniMed) throws SQLException {
//        String sqlUltimo = "SELECT MAX(ID_UNIMEDIDA)+1 AS ULTIMO FROM CAD_UNIMEDIDA";
//        super.ps = super.conexao.prepareStatement(sqlUltimo);
//        super.executeSQL();
//        uniMed.setIdUniMedida(super.resultSet.getInt("ULTIMO"));
        uniMed.setIdUniMedida(new UltimoCodigo().getUltimo("CAD_UNIMEDIDA", "ID_UNIMEDIDA"));

        String SQL = "insert into CAD_UNIMEDIDA("
                + "id_unimedida,ds_sigla,sigla) "
                + "values(?,?,?)";

        super.ps = super.conexao.prepareStatement(SQL);

        super.ps.setInt(1, uniMed.getIdUniMedida());
        super.ps.setString(2, uniMed.getDsSigla());
        try {
            super.ps.setString(3, uniMed.getSigla());
        } catch (NullPointerException e) {
            super.ps.setNull(3, Types.VARCHAR);
        }

        
        super.insert();
        super.ps.close();
    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }

    public UnidadeMedida findOne(UnidadeMedida obj) throws SQLException {
        String sql = "select * from cad_unimedida where id_unimedida = ?";

        super.ps = super.conexao.prepareStatement(sql);
        super.ps.setInt(1, obj.getIdUniMedida());
        super.executeSQL();
        return getObjeto(super.resultSet);

    }

    public ResultSet resultUniMedida() throws SQLException {
        String sql = "select * from cad_unimedida";

        super.ps = super.conexao.prepareStatement(sql);
        super.executeSQL();
        return super.resultSet;

    }

    public List<UnidadeMedida> getAll() {

        try {
            String SQL = "SELECT ID_UNIMEDIDA,DS_SIGLA,SIGLA FROM CAD_UNIMEDIDA ORDER BY ID_UNIMEDIDA";
            super.ps = super.conexao.prepareStatement(SQL);
            super.executeSQL();
            List<UnidadeMedida> ret = new ArrayList<>();

            while (super.resultSet.next()) {
                UnidadeMedida uf = new UnidadeMedida();
                uf.setIdUniMedida(super.resultSet.getInt("ID_UNIMEDIDA"));
                uf.setDsSigla(super.resultSet.getString("DS_SIGLA"));
                uf.setSigla(super.resultSet.getString("SIGLA"));

                ret.add(uf);
            }

            return ret;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public UnidadeMedida buscarPorCodigo(Integer cod) throws SQLException {
        try {

            String SQL = "SELECT * FROM CAD_UNIMEDIDA WHERE ID_UNIMEDIDA= " + cod;

            super.c.executeSQL(SQL);
            if (super.c.resultSet.first()) {
                UnidadeMedida ret = new UnidadeMedida();
                ret.setIdUniMedida(super.c.resultSet.getInt("ID_UNIMEDIDA"));
                ret.setDsSigla(super.c.resultSet.getString("DS_SIGLA"));
                ret.setSigla(super.c.resultSet.getString("SIGLA"));
                return ret;

            } else {

                return null;
            }

        } catch (Exception ex) {
            ex.printStackTrace();

            return null;
        }
    }

    public ResultSet findById(Integer cod) throws SQLException {
       

            String SQL = "SELECT * FROM CAD_UNIMEDIDA WHERE ID_UNIMEDIDA= " + cod;
            super.ps = super.conexao.prepareStatement(SQL);
            super.executeSQL();

            return super.resultSet;
        
    }

    public UnidadeMedida getObjeto(ResultSet resultSet) throws SQLException {

        UnidadeMedida unid = new UnidadeMedida();
        if (resultSet.next()) {
            unid.setIdUniMedida(resultSet.getInt("ID_UNIMEDIDA"));
            unid.setDsSigla(resultSet.getString("DS_SIGLA"));
            unid.setSigla(resultSet.getString("SIGLA"));
            return unid;
        }
        return null;

    }

    public void findAll() {

    }

    public ResultSet getByDesc(String descricao) throws SQLException {
        try {

            String SQL = "SELECT ID_UNIMEDIDA, DS_SIGLA, SIGLA FROM CAD_UNIMEDIDA WHERE DS_SIGLA LIKE '%" + descricao + "%'";
            super.ps = super.conexao.prepareStatement(SQL);
            super.executeSQL();

            return super.resultSet;
        } catch (Exception ex) {

            return null;
        }
    }

    public static void main(String[] args) throws SQLException {
        UnidadeMedida uni = new UnidadeMedida();
        UnidadeMedidaDao uniDao = new UnidadeMedidaDao();

        uni.setIdUniMedida(1);
        uni.setDsSigla("Metros");
        uni.setSigla("MT");

        uniDao.insert(uni);
    }

}
