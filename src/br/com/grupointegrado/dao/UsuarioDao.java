package br.com.grupointegrado.dao;

import br.com.grupointegrado.model.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author leonardo
 */
public class UsuarioDao extends Dao {

    private StringBuffer SQL = new StringBuffer();

    public Usuario getUser(String nmLogin, String pwLogin) {

        PreparedStatement ps = null;
        try {
            SQL.setLength(0);
            SQL.append("SELECT ID, \n");
            SQL.append("  NOME, \n");
            SQL.append("  LOGIN, \n");
            SQL.append("  SENHA, \n");
            SQL.append("  IN_SITUACAO\n");
            SQL.append("FROM CAD_USUARIO \n");
            SQL.append("WHERE LOGIN LIKE ? \n");
            SQL.append("AND SENHA LIKE ? \n");
            SQL.append("AND IN_SITUACAO LIKE ?");

            super.ps = super.conexao.prepareStatement(SQL.toString());
            super.ps.setString(1, nmLogin);
            super.ps.setString(2, pwLogin);
            super.ps.setString(3, "A");

            super.executeSQL();
            
            if (super.resultSet.next()) {
            Usuario ret = new Usuario();
            ret.setCdUsuario(super.resultSet.getInt("ID"));
            ret.setNmUsuario(super.resultSet.getString("NOME"));
            ret.setNmLogin(super.resultSet.getString("LOGIN"));
            ret.setPwLogin(super.resultSet.getString("SENHA"));
            ret.setInSituacao(super.resultSet.getString("IN_SITUACAO"));
            return ret;
            }else{
                return null;
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        } 

    }

    public static void main(String[] args) {

        UsuarioDao uDao = new UsuarioDao();
        System.out.println(uDao.getUser("leonardo", "leo123"));

    }

}
