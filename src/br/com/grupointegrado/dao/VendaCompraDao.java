
package br.com.grupointegrado.dao;

import br.com.grupointegrado.model.Cliente;
import br.com.grupointegrado.model.VendaCompra;
import br.com.grupointegrado.util.UltimoCodigo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author monteiro
 */
public class VendaCompraDao extends Dao{
    
    private VendaCompraItemDao iDao;
    private ClienteDao cDao;

    public VendaCompraDao() throws ClassNotFoundException, SQLException {
        iDao = new VendaCompraItemDao();
    }
    
    public void insert(VendaCompra obj) throws SQLException{
        
        obj.setNrSequencia(new UltimoCodigo().getUltimo("MOV_VENDACOMPRA", "NR_SEQUENCIA"));
        String sql = "INSERT INTO \"TRABPOO\".\"MOV_VENDACOMPRA\" "
                + "(NR_SEQUENCIA, CD_CLIENTE, IN_SITUACAO, VL_VENDA, VL_DESCONTO, VL_TOTAL, DATA_VENDA) "
                + "VALUES (?,?,?,?,?,?,?))";
        super.ps = super.conexao.prepareStatement(sql);
        
        super.ps.setInt(1, obj.getNrSequencia());
        super.ps.setInt(2, obj.getCliente().getCdCliente());
        super.ps.setInt(3, obj.getInSituacao());
        super.ps.setDouble(4, obj.getVlVenda());
        super.ps.setDouble(5, obj.getVlDesconto());
        super.ps.setDouble(6, obj.getVlTotal());
        super.ps.setDate(7, (java.sql.Date) new Date(obj.getDtVenda().getTime()));
        
        super.insert();
        super.ps.close();
//        for (VendaCompraItem item : obj.getItens()) {
//            iDao.insert(item);
//        }
        
    }
    public void update (VendaCompra vCompra){
         String sql ="update  mov_vendacompra set "
                 + "cd_cliente = ?, "
                 + "in_situacao = ?, "
                 + "vl_venda = ?, "
                 + "vl_desconto = ?,"
                 + "vl_total = ?,"
                 + "data_venda = ?";
    }
    public ResultSet findAll() throws SQLException {
        String sql = "SELECT * FROM MOV_VENDACOMPRA";
        
        super.ps = super.conexao.prepareStatement(sql);
        
        super.executeSQL();
  
        return super.resultSet;
    }
    public VendaCompra findOne(VendaCompra vc) throws SQLException, Exception {
       
        String sql = "select * from MOV_VENDACOMPRA WHERE NR_SEQUENCIA = ? ";
        super.ps = super.conexao.prepareStatement(sql);
        super.ps.setInt(1, vc.getNrSequencia());
        super.executeSQL();

        //SE EXISTIR A PRIMEIRA POSICAO NO CASO O NRSEQUENCIA DO WHERE 
        if (super.resultSet.next()) {
            return getObject(super.resultSet);
        }

        return null;
    }
   private VendaCompra getObject(ResultSet rs) throws Exception {

        VendaCompra vc = new VendaCompra();
        vc.setNrSequencia(rs.getInt("NR_SEQUENCIA"));
        vc.setInSituacao(rs.getInt("IN_SITUACAO"));
        vc.setVlVenda(rs.getDouble("VL_VENDA"));
        vc.setVlDesconto(rs.getDouble("VL_DESCONTO"));
        vc.setVlTotal(rs.getDouble("VL_TOTAL"));
        vc.setDtVenda(new Date(rs.getDate("DATA_VENDA").getTime()));

        Cliente cli = new Cliente();
        cli.setCdCliente(rs.getInt("CD_CLIENTE"));
        cli = (Cliente) cDao.findOne(cli);
        vc.setCliente(cli);

        return vc;
    }
    
}
