
package br.com.grupointegrado.dao;

import br.com.grupointegrado.model.VendaCompraItem;
import java.sql.SQLException;

/**
 *
 * @author monteiro
 */
public class VendaCompraItemDao extends Dao{

    public VendaCompraItemDao() throws ClassNotFoundException, SQLException {
    
    }
    
   public void insert(VendaCompraItem obj) throws SQLException{
        
        String sql = "INSERT INTO MOV_ITEM_VENDACOMPRA (NR_SEQUENCIA,"
                + "CD_PRODUTO,"
                + "QUANTIDADE,"
                + "VL_ITEM,"
                + "VL_DESCONTO) "
                + "VALUES(?,?,?,?,?,?,?)";
        
        super.ps = super.conexao.prepareStatement(sql);
        
        super.ps.setInt(1, obj.getVenda().getNrSequencia());
        super.ps.setInt(2, obj.getProduto().getIdProduto());
        super.ps.setInt(3, obj.getProduto().getIdProduto());
        super.ps.setInt(4, obj.getProduto().getIdProduto());
        super.ps.setInt(5, obj.getProduto().getIdProduto());
        super.ps.setInt(6, obj.getProduto().getIdProduto());
        super.ps.setInt(7, obj.getProduto().getIdProduto());
        
    }
    
}
