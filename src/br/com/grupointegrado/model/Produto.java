/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.model;

/**
 *
 * @author Leonardo
 */
public class Produto {
    private Integer idProduto;
    private UnidadeMedida idUniMedida;
    private String descProduto;
    private Double vlUnitario;
    private Double qtEstoque;

    public Produto() {
    }

    public Produto(Integer idProduto, UnidadeMedida idUniMedida, String descProduto, Double vlUnitario, Double qtEstoque) {
        this.idProduto = idProduto;
        this.idUniMedida = idUniMedida;
        this.descProduto = descProduto;
        this.vlUnitario = vlUnitario;
        this.qtEstoque = qtEstoque;
    }

    public Integer getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    public UnidadeMedida getIdUniMedida() {
        return idUniMedida;
    }

    public void setIdUniMedida(UnidadeMedida idUniMedida) {
        this.idUniMedida = idUniMedida;
    }

    public String getDescProduto() {
        return descProduto;
    }

    public void setDescProduto(String descProduto) {
        this.descProduto = descProduto;
    }

    public Double getVlUnitario() {
        return vlUnitario;
    }

    public void setVlUnitario(Double vlUnitario) {
        this.vlUnitario = vlUnitario;
    }

    public Double getQtEstoque() {
        return qtEstoque;
    }

    public void setQtEstoque(Double qtEstoque) {
        this.qtEstoque = qtEstoque;
    }

    @Override
    public String toString() {
        return descProduto;
    }
    
    
}
