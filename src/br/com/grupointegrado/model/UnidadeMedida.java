/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.model;

/**
 *
 * @author Leonardo
 */
public class UnidadeMedida {
    private Integer idUniMedida;
    private String dsSigla;
    private String Sigla;

    public UnidadeMedida() {
    }

    public UnidadeMedida(Integer idUniMedida, String dsSigla, String Sigla) {
        this.idUniMedida = idUniMedida;
        this.dsSigla = dsSigla;
        this.Sigla = Sigla;
    }

    public Integer getIdUniMedida() {
        return idUniMedida;
    }

    public void setIdUniMedida(Integer idUniMedida) {
        this.idUniMedida = idUniMedida;
    }

    public String getDsSigla() {
        return dsSigla;
    }

    public void setDsSigla(String dsSigla) {
        this.dsSigla = dsSigla;
    }

    public String getSigla() {
        return Sigla;
    }

    public void setSigla(String Sigla) {
        this.Sigla = Sigla;
    }

    @Override
    public String toString() {
        return dsSigla;
    }
    
    
}
