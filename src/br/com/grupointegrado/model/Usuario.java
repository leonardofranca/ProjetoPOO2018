package br.com.grupointegrado.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author leonardo
 */
public class Usuario {

    private Integer cdUsuario;
    private String nmUsuario;
    private String nmLogin;
    private String pwLogin;
    private String inSituacao;

    public Usuario() {
    }

    public Usuario(Integer cdUsuario, String nmUsuario, String nmLogin, String pwLogin, String inSituacao) {
        this.cdUsuario = cdUsuario;
        this.nmUsuario = nmUsuario;
        this.nmLogin = nmLogin;
        this.pwLogin = pwLogin;
        this.inSituacao = inSituacao;
    }

    public Integer getCdUsuario() {
        return cdUsuario;
    }

    public void setCdUsuario(Integer cdUsuario) {
        this.cdUsuario = cdUsuario;
    }

    public String getNmUsuario() {
        return nmUsuario;
    }

    public void setNmUsuario(String nmUsuario) {
        this.nmUsuario = nmUsuario;
    }

    public String getNmLogin() {
        return nmLogin;
    }

    public void setNmLogin(String nmLogin) {
        this.nmLogin = nmLogin;
    }

    public String getPwLogin() {
        return pwLogin;
    }

    public void setPwLogin(String pwLogin) {
        this.pwLogin = pwLogin;
    }

    public String getInSituacao() {
        return inSituacao;
    }

    public void setInSituacao(String inSituacao) {
        this.inSituacao = inSituacao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.cdUsuario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.cdUsuario, other.cdUsuario)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CadUsuario{" + "cdUsuario=" + cdUsuario + ", nmUsuario=" + nmUsuario + ", nmLogin=" + nmLogin + ", pwLogin=" + pwLogin + ", inAtivo=" + '}';
    }

}
