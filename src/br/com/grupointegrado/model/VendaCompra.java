package br.com.grupointegrado.model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author monteiro
 */
public class VendaCompra {

    private Integer nrSequencia;
    private Cliente cliente;
    private Integer inSituacao;
    private Double vlVenda;
    private Double vlDesconto;
    private Double vlTotal;
    private List<VendaCompraItem> itens;
    private Date dtVenda;

    public Date getDtVenda() {
        return dtVenda;
    }

    public void setDtVenda(Date dtVenda) {
        this.dtVenda = dtVenda;
    }
    

    public List<VendaCompraItem> getItens() {
        return itens;
    }

    public void setItens(List<VendaCompraItem> itens) {
        this.itens = itens;
    }

    public Integer getNrSequencia() {
        return nrSequencia;
    }

    public void setNrSequencia(Integer nrSequencia) {
        this.nrSequencia = nrSequencia;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getInSituacao() {
        return inSituacao;
    }

    public void setInSituacao(Integer inSituacao) {
        this.inSituacao = inSituacao;
    }

    public Double getVlVenda() {
        return vlVenda;
    }

    public void setVlVenda(Double vlVenda) {
        this.vlVenda = vlVenda;
    }

    public Double getVlDesconto() {
        return vlDesconto;
    }

    public void setVlDesconto(Double vlDesconto) {
        this.vlDesconto = vlDesconto;
    }

    public Double getVlTotal() {
        return vlTotal;
    }

    public void setVlTotal(Double vlTotal) {
        this.vlTotal = vlTotal;
    }

}
