
package br.com.grupointegrado.model;

/**
 *
 * @author monteiro
 */
public class VendaCompraItem {
    
    private VendaCompra venda;
    private Produto produto;
    private Double quantidade;
    private Double vlItem;
    private Double vlDesconto;

    public VendaCompra getVenda() {
        return venda;
    }

    public void setVenda(VendaCompra venda) {
        this.venda = venda;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Double getVlItem() {
        return vlItem;
    }

    public void setVlItem(Double vlItem) {
        this.vlItem = vlItem;
    }

    public Double getVlDesconto() {
        return vlDesconto;
    }

    public void setVlDesconto(Double vlDesconto) {
        this.vlDesconto = vlDesconto;
    }
    
    
    
    
    
    
}
