/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.util;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JTextField;

/**
 *
 * @author Leonardo
 */
public class LimparCampos {
    
    public void limparCampos(Container comp1){
        
        Component components[] = comp1.getComponents();
        for(Component component : components){
            
            if (component instanceof JTextField) {
                
                
                JTextField field = (JTextField) component;
                field.setText("");
                
               
            }
        }
    }
}
