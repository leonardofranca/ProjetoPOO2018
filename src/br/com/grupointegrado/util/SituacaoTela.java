/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.util;

/**
 *
 * @author Leonardo
 */
public class SituacaoTela {
    
    public static final int NOVO = 1;
    public static final int ALTERAR = 2;
    public static final int EXCLUIR = 3;
    public static final int SELECIONADO = 4;
    public static final int CONSULTAR = 5;
    public static final int PADRAO = 0;
    
}
