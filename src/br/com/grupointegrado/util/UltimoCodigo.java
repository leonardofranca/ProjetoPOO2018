/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.util;

import br.com.grupointegrado.dao.Dao;
import java.sql.SQLException;

/**
 *
 * @author Leonardo
 */
public class UltimoCodigo extends Dao {

    public Integer getUltimo(String tabela, String nomeColuna) {
        try {
            String sqlUltimo = "SELECT COALESCE(MAX(" + nomeColuna + "),0)+1 as ULTIMO FROM " + tabela;
            super.ps = super.conexao.prepareStatement(sqlUltimo);
            super.executeSQL();

            super.resultSet.next();
            Integer retorno = super.resultSet.getInt("ULTIMO");
            super.ps.close();
            return retorno;

        } catch (SQLException ex) {
            ex.printStackTrace();

            return null;
        }

    }
}
