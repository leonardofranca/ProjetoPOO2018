/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.util;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 *
 * @author rodrigo
 */
public class ValidarCampos {
    
    public void validaCamposIncluir(Container container, Container container1) {
        Component components[] = container.getComponents();
        
        for (Component component : components) {
            
            if (component instanceof JTextField) {
                JTextField field = (JTextField) component;
                // if (field.isEditable()) {
                field.setEnabled(true);
                //}
                
            }
            if (component instanceof JComboBox) {
                JComboBox combo = (JComboBox) component;
                combo.setEnabled(true);
            }
        }
        
        Component componentBotoes[] = container1.getComponents();
        for (Component component1 : componentBotoes) {
            
            if (component1 instanceof JButton) {
                JButton field = (JButton) component1;
                if (field.getText().equals("Novo")) {
                    field.setEnabled(false);
                }
                
                if (field.getText().equals("Alterar")) {
                    field.setEnabled(false);
                }
                
                if (field.getText().equals("Excluir")) {
                    field.setEnabled(false);
                }
                
                if (field.getText().equals("Gravar")) {
                    field.setEnabled(true);
                }
                
                if (field.getText().equals("Cancelar")) {
                    field.setEnabled(true);
                }
            }
            
        }
    }
    
    public void validaCamposCancelar(Container container, Container container1) { //Painel , Botoes
        Component components[] = container.getComponents();
        
        for (Component component : components) {
            
            if (component instanceof JTextField) {
                
                JTextField field = (JTextField) component;
                //if (field.isEditable()) {
                field.setEnabled(false);

                //}
        
            }
            if (component instanceof JComboBox) {
                JComboBox combo = (JComboBox) component;
                combo.setEnabled(false);
            }
        }
        
        Component componentBotoes[] = container1.getComponents();
        for (Component component1 : componentBotoes) {
            
            if (component1 instanceof JButton) {
                JButton field = (JButton) component1;
                if (field.getText().equals("Novo")) {
                    field.setEnabled(true);
                }
                
                if (field.getText().equals("Alterar")) {
                    field.setEnabled(true);
                }
                
                if (field.getText().equals("Excluir")) {
                    field.setEnabled(true);
                }
                
                if (field.getText().equals("Gravar")) {
                    field.setEnabled(false);
                }
                
                if (field.getText().equals("Cancelar")) {
                    field.setEnabled(false);
                }
            }
            
        }
    }
    
}
