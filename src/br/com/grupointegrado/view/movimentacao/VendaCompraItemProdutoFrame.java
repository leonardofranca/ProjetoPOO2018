/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.grupointegrado.view.movimentacao;

import br.com.grupointegrado.dao.ProdutoDao;
import br.com.grupointegrado.model.Produto;
import br.com.grupointegrado.model.VendaCompra;
import br.com.grupointegrado.model.VendaCompraItem;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Leonardo
 */
public class VendaCompraItemProdutoFrame extends javax.swing.JDialog {

    /**
     * Creates new form VendaCompraItemProdutoFrame
     */
    
    private ProdutoDao proDao;
    private Produto produto;
    public VendaCompraItem venda;
    public VendaCompraItemProdutoFrame() {
        initComponents();
        try {
            proDao = new ProdutoDao();
            produto = new Produto();
        } catch (Exception e) {
        }
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTFCodProduto = new javax.swing.JTextField();
        jTFDescricao = new javax.swing.JTextField();
        jTFQuantidade = new javax.swing.JTextField();
        jTValorBruto = new javax.swing.JTextField();
        jTFValorDesconto = new javax.swing.JTextField();
        jTFValorLiquido = new javax.swing.JTextField();
        jTValorUnitario = new javax.swing.JTextField();
        jBCancelar = new javax.swing.JButton();
        jBAdicionar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTFCodProduto.setBorder(javax.swing.BorderFactory.createTitledBorder("Produto"));
        jTFCodProduto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTFCodProdutoFocusLost(evt);
            }
        });

        jTFDescricao.setEditable(false);
        jTFDescricao.setBorder(javax.swing.BorderFactory.createTitledBorder("Descricao"));
        jTFDescricao.setEnabled(false);

        jTFQuantidade.setBorder(javax.swing.BorderFactory.createTitledBorder("Quantidade"));

        jTValorBruto.setEditable(false);
        jTValorBruto.setBorder(javax.swing.BorderFactory.createTitledBorder("Valor Bruto"));
        jTValorBruto.setEnabled(false);

        jTFValorDesconto.setBorder(javax.swing.BorderFactory.createTitledBorder("Valor Desconto"));

        jTFValorLiquido.setEditable(false);
        jTFValorLiquido.setBorder(javax.swing.BorderFactory.createTitledBorder("Valor Liquido"));
        jTFValorLiquido.setEnabled(false);

        jTValorUnitario.setBorder(javax.swing.BorderFactory.createTitledBorder("Valor Unitario"));

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });

        jBAdicionar.setText("Adicionar");
        jBAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAdicionarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTFDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 388, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jBAdicionar)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTFQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTValorBruto, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTFValorDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jBCancelar)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jTFValorLiquido))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTFQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTValorBruto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFValorDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFValorLiquido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAdicionar)
                    .addComponent(jBCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(523, 195));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTFCodProdutoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTFCodProdutoFocusLost
        try {
            if (!jTFCodProduto.getText().equals("")) {
                Produto pro = new Produto();
                pro.setIdProduto(Integer.parseInt(jTFCodProduto.getText().trim()));
                Produto retorno = proDao.findById(pro.getIdProduto());
                setCampos(retorno);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Erro");
        }
    }//GEN-LAST:event_jTFCodProdutoFocusLost

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        produto = new Produto();
        
        this.dispose();
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jBAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAdicionarActionPerformed

        if(validaCampos()){
            
            return;
        }
        venda = getProduto();
        this.dispose();
    }//GEN-LAST:event_jBAdicionarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VendaCompraItemProdutoFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VendaCompraItemProdutoFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VendaCompraItemProdutoFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VendaCompraItemProdutoFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VendaCompraItemProdutoFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBAdicionar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JTextField jTFCodProduto;
    private javax.swing.JTextField jTFDescricao;
    private javax.swing.JTextField jTFQuantidade;
    private javax.swing.JTextField jTFValorDesconto;
    private javax.swing.JTextField jTFValorLiquido;
    private javax.swing.JTextField jTValorBruto;
    private javax.swing.JTextField jTValorUnitario;
    // End of variables declaration//GEN-END:variables
    
    private void setCampos (Produto pro){
        jTFDescricao.setText(pro.getDescProduto());
        jTValorUnitario.setText(String.valueOf(pro.getVlUnitario()));
        jTFQuantidade.setText("1.00");
        jTFValorDesconto.setText("0.00");
        
        calculaTotal();
        
    }
    
   

    private void calculaTotal() {
        Double ValorUnitario = Double.parseDouble(jTValorUnitario.getText());
        Double quantidade = Double.parseDouble(jTFQuantidade.getText());
        Double valorDesconto = Double.parseDouble(jTFValorDesconto.getText());
        
        Double total = ValorUnitario * quantidade;
        
        jTValorBruto.setText(String.valueOf(total));
        total = total - valorDesconto;
        
        jTValorBruto.setText(String.valueOf(total));
    }
    
    private Boolean validaCampos(){
        if(jTFCodProduto.getText().trim().equals("")){
            JOptionPane.showMessageDialog(this, "Selecione um Produto");
            jTFCodProduto.grabFocus();
            return true;
        }
        if(jTValorUnitario.getText().trim().equals("")){
            JOptionPane.showMessageDialog(this, "Informe o Valor Unitario");
            jTValorUnitario.grabFocus();
            return true;
        }
        if(jTFQuantidade.getText().trim().equals("")){
            JOptionPane.showMessageDialog(this, "Informe a Quantidade");
            jTFQuantidade.grabFocus();
            return true;
        }
        if(jTFValorDesconto.getText().trim().equals("")){
            JOptionPane.showMessageDialog(this, "Informe o Valor do Desconto");
            jTFValorDesconto.grabFocus();
            return true;
        }
        
            
        return false;
    }
    private VendaCompraItem getProduto(){
        VendaCompraItem vend = new VendaCompraItem();
        VendaCompra compra = new VendaCompra();
        Produto pro = new Produto();
        pro.setIdProduto(Integer.parseInt(jTFCodProduto.getText().trim()));
        pro.setDescProduto(jTFDescricao.getText());
        
        vend.setProduto(pro);
        vend.setVlItem(Double.parseDouble(jTValorUnitario.getText()));
        vend.setVlDesconto(Double.parseDouble(jTFValorDesconto.getText()));
        vend.setQuantidade(Double.parseDouble(jTFQuantidade.getText()));
        
        
        return vend;
        
    }
}
